"use strict";

var assert = require("assert");
const { Flow } = require("core");
const { Converter: Mv2Converter } = require("mv2");
const { Converter: Mv3Converter } = require("mv3");

describe("Core", function() {
    it("uses converter for Manifest V2 (from mv2 module)", function() {
        let converter = new Mv2Converter();
        let flow = new Flow(converter);
        assert.equal("test123_mv2", flow.doSomething('{ "something": "test123" }'));
    });

    it("uses converter for Manifest V3 (from mv3 module)", function () {
        let convertor = new Mv3Converter();
        let flow = new Flow(convertor);
        assert.equal("test123_mv3", flow.doSomething('{ "something": "test123" }'));
    });
});