"use strict";

let { Filter } = require("filters");

let Flow =
exports.Flow = class Flow {
    constructor(converter) {
        this.converter = converter;
    }

    doSomething(text) {
        let abpFilter = Filter.fromText(text);
        // assuming there is an agreement on `convertor` interface:
        // eg. `convert()` that accepts filter class instance and returns string/anything.
        // the actual impls are in separate modules (see `mv2`, `mv3` module)
        // which are injected by the consumer (see `sdk` module).
        return this.converter.convert(abpFilter);
    }
}